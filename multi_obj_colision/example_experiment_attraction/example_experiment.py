from multi_obj_colision.example_experiment_attraction.example_experiment_spec import ExampleExperimentAttractionSpec
from multi_obj_colision.graphical.pygame_hnadler import PygameHandler
from multi_obj_colision.physical.data_collector import DataCollector
import matplotlib.pyplot as plt

# def potential_energy_calculator(x,y,mass):
#     return y*mass*9.81

def ineraction_calculator(x1,y1,m1,x2,y2,m2):
    def formula(c1,c2,m1,m2):
        R = c1-c2
        G = 6.67*1e1 # should be 1e-11
        zer_n = R < 1e-10 
        zer_p = R > -1e-10
        zer = zer_n*zer_p

        result = G
        result *=m1*m2
        result/=R**2+zer

        # direction coefficient is determined with respect to obj 1
        direction = c2>c1
        direction = 1 * direction + (-1)*(1-direction)

        return result*(1-zer)*direction
    Fx = formula(x1,x2,m1,m2)
    Fy = formula(y1,y2,m1,m2)
    return Fx, Fy
def run():
    exp_spec = ExampleExperimentAttractionSpec(ineraction_calculator)
    dc = DataCollector()
    exp_spec.set_data_collector(dc)
    ph = PygameHandler(experiment=exp_spec,time_speed_factor=0.1,zoom_factor=25,limit_fps=True)
    ph.graphics_loop()
    fig, ax = plt.subplots(2,4)
    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1, wspace=0.4, hspace=0.4)
    dc.plot_x_velocity(ax[0][0])
    dc.plot_y_velocity(ax[1][0])
    dc.plot_x(ax[0][1])
    dc.plot_y(ax[1][1])

    dc.plot_trajectory(ax[0][2])
    dc.plot_total_kinetic_energy(ax[1][2], exp_spec.get_obj_masses())

    # dc.plot_total_potential_energy(ax[0][3], exp_spec.get_obj_masses(),potential_energy_calculator)
    # dc.plot_total_energy_of_system(ax[1][3], exp_spec.get_obj_masses(),potential_energy_calculator)

    plt.show(block=True)



    