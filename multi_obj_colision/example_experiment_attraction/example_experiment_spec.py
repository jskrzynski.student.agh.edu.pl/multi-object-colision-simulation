from typing import Iterable, Tuple, Union
from multi_obj_colision.physical.experiment_spec import ExperimentSpec, SimulationStepType
from multi_obj_colision.physical.multi_object_colision import ObjectsSet, SystemBorders


class ExampleExperimentAttractionSpec(ExperimentSpec):

    def __init__(self,calculator) -> None:
        obj_set = ObjectsSet(calculator=calculator)
        radius = [1,3]
        mass = [10,40]
        x = [2,12]
        y = [4,4]
        vx = [0,0]
        vy = [0,0]
        obj_set.add_object(radius,mass,x,y,vx,vy)
        super().__init__(obj_set)

    def _is_next_step_with_acceleration(self, step_id:int)->SimulationStepType:
        """
        Do we accelerate in `step_id` step? (acceeration can be negative)
        And hov the acceleration is computed
        """
        return SimulationStepType.NO_ACCELERATION

    
    # def _get_acceleration_value(self, step_id:int)->Tuple[Union[Iterable,float], Union[Iterable,float]]:
    #     """
    #     Returns tuple (ax,ay) of iterables containing x and y components of acceleration
    #     acting in the given timestep

    #     May not be implemented if system accelerates only by force
    #     """
    #     return (0,-10) # gravity

    # def _get_force_value(self, step_id:int)->Tuple[Iterable, Iterable]:
    #     """
    #     Returns tuple (fx,fy) of iterables containing x and y components of force
    #     acting in the given timestep

    #     May not be implemented if system accelerates only by acceleration
    #     """
    #     raise NotImplemented


    def _get_dt_value(self, step_id:int)->float:
        """
        Returns time difference for the given timestep
        """
        return 0.01

    def _get_system_borders(self, step_id:int)->SystemBorders:
        """
        Returns the borders of a system in the given timestep

        When running in graphics mode it would be easier to have it as constant value across timeline
        """
        return SystemBorders(10,25,0,0)