
import abc
import copy
from enum import Enum
from typing import Iterable, Tuple, Union
from multi_obj_colision.physical.data_collector import DataCollector
from multi_obj_colision.physical.multi_object_colision import ObjectsSet, SystemBorders

class SimulationStepType(Enum):
    ACCELERATION_BY_NUMBER = 1
    ACCELERATION_BY_FORCE  = 2
    NO_ACCELERATION        = 0

class ExperimentSpec(abc.ABC):
    """
    It is a base class that should be used as an iterface for further
    developement - it is a wrapper around the numerical simulation
    that is responsible for delivering next steps of simulation and 
    collecting intermediate results for analysis
    """

    def __init__(self, obj_set: ObjectsSet) -> None:
        super().__init__()
        self.obj_set = obj_set
        self.step_counter = 0 # steps performed
        self.time_passed = 0
        self.register_data = False
        self.data_collector=None

    def set_data_collector(self, dc: DataCollector, enable_collection:bool= True):
        self.register_data = enable_collection
        self.data_collector = dc

    def set_collect_data(self, do_collect:bool):
        self.register_data = do_collect
        self._run_data_collector()

    def _run_data_collector(self):
        if self.register_data:
            vx = copy.deepcopy(self.obj_set.vx)
            vy = copy.deepcopy(self.obj_set.vy)
            dt = self.get_current_dt()
            
            x = copy.deepcopy(self.obj_set.x)
            y = copy.deepcopy(self.obj_set.y)

            self.data_collector.register_data(x,y,vx,vy,dt,self.time_passed*1000)
    
    def next_stage(self)->None:
        """
        Function advances simulation by a single step, according to specs
        saved in object state
        - makes
        """
        self.step_counter+=1
        step_type = self._is_next_step_with_acceleration(self.step_counter)
        boundaries = self._get_system_borders(self.step_counter)
        dt = self._get_dt_value(self.step_counter)
        self.time_passed+=dt
        if step_type == SimulationStepType.ACCELERATION_BY_NUMBER:
            ax,ay = self._get_acceleration_value(self.step_counter)
            self.obj_set.pipeline_timestep_by_acceleration(dt,ax,ay,boundaries)
        elif step_type == SimulationStepType.ACCELERATION_BY_FORCE:
            fx,fy = self._get_force_value(self.step_counter)
            self.obj_set.pipeline_timestep_by_force(dt,fx,fy,boundaries)
        elif step_type == SimulationStepType.NO_ACCELERATION:
            self.obj_set.pipeline_timestep_by_acceleration(dt,0,0,boundaries)
        self._run_data_collector()

    def get_object_coords(self)->Tuple[Iterable,Iterable]:
        """
        Returns tuple (x,y) where both are iterables
        """
        x = copy.deepcopy(self.obj_set.x)
        y = copy.deepcopy(self.obj_set.y)
        return (x,y)

    def get_object_radius(self)->Iterable:
        """
        Returns radiuses of objects as an iterable
        """
        r = copy.deepcopy(self.obj_set.r)
        return r

    def get_obj_masses(self):
        m = copy.deepcopy(self.obj_set.m)
        return m

    def get_current_borders(self)->SystemBorders:
        return self._get_system_borders(self.step_counter)

    def get_current_dt(self)->float:
        return self._get_dt_value(self.step_counter)

    # ##############################################################################
    # Function to be implemented by user

    @abc.abstractmethod
    def _is_next_step_with_acceleration(self, step_id:int)->SimulationStepType:
        """
        Do we accelerate in `step_id` step? (acceeration can be negative)
        And hov the acceleration is computed
        """
        pass

    
    def _get_acceleration_value(self, step_id:int)->Tuple[Union[Iterable,float], Union[Iterable,float]]:
        """
        Returns tuple (ax,ay) of iterables containing x and y components of acceleration
        acting in the given timestep

        May not be implemented if system accelerates only by force
        """
        raise NotImplemented

    def _get_force_value(self, step_id:int)->Tuple[Union[Iterable,float], Union[Iterable,float]]:
        """
        Returns tuple (fx,fy) of iterables containing x and y components of force
        acting in the given timestep

        May not be implemented if system accelerates only by acceleration
        """
        raise NotImplemented


    @abc.abstractmethod
    def _get_dt_value(self, step_id:int)->float:
        """
        Returns time difference for the given timestep
        """
        pass

    @abc.abstractmethod
    def _get_system_borders(self, step_id:int)->SystemBorders:
        """
        Returns the borders of a system in the given timestep

        When running in graphics mode it would be easier to have it as constant value across timeline
        """
        pass