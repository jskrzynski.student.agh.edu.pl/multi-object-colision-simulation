from typing import Callable, List
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.axes import Axes

class DataCollector:
    _x_history: List[np.ndarray]
    _y_history: List[np.ndarray]

    _vx_history: List[np.ndarray]
    _vy_history: List[np.ndarray]
    _v_abs_history: List[np.ndarray]

    _distance_travelled: List[np.ndarray]
    _distance_travelled_x: List[np.ndarray]
    _distance_travelled_y: List[np.ndarray]

    _time_labels_ms: List[int]

    _total_potential_energy_of_object: List[np.ndarray]

    def __init__(self, record_limit:int=100000) -> None:
        self._x_history = []
        self._y_history = []

        self._vx_history = []
        self._vy_history = []
        self._v_abs_history = []

        self._distance_travelled_x = []
        self._distance_travelled_y = []
        self._distance_travelled = []

        self._time_labels_ms = []

        self._record_limit = record_limit
        self._record_counter = 0


    def register_data(self,x,y,vx:np.ndarray,vy:np.ndarray,dt,time_of_event_ms):
        if self._record_limit > self._record_counter:
            self._record_counter+=1

            # That would work only for the uniform motion or very small steps
            # sx = vx*dt
            # sy = vy*dt
            # self._distance_travelled_x.append(sx)
            # self._distance_travelled_y.append(sy)

            # s = (sx**2 + sy**2)**0.5
            # self._distance_travelled.append(s)

            self._x_history.append(x)
            self._y_history.append(y)

            self._vx_history.append(vx)
            self._vy_history.append(vy)

            v = (vx**2+vy**2)**0.5
            self._v_abs_history.append(v)

            self._time_labels_ms.append(time_of_event_ms)

    def plot_x_velocity(self, ax:Axes):
        ax.plot(self._time_labels_ms, self._vx_history)
        ax.set_title("X velocity of objects")
        ax.set_xlabel("Time [ms]")
        ax.set_ylabel("Velocity [m/s]")
        
    def plot_y_velocity(self, ax:Axes):
        ax.plot(self._time_labels_ms, self._vy_history)
        ax.set_title("Y velocity of objects")
        ax.set_xlabel("Time [ms]")
        ax.set_ylabel("Velocity [m/s]")

    def plot_x(self, ax:Axes):
        ax.plot(self._time_labels_ms, self._x_history)
        ax.set_title("X coordinate of objects")
        ax.set_xlabel("Time [ms]")
        ax.set_ylabel("X [m]")
        
    def plot_y(self, ax:Axes):
        ax.plot(self._time_labels_ms, self._y_history)
        ax.set_title("Y coordinate of objects")
        ax.set_xlabel("Time [ms]")
        ax.set_ylabel("Y [m]")

    def plot_trajectory(self, ax:Axes):
        ax.plot(self._x_history, self._y_history)
        ax.set_title("Trajectory")
        ax.set_xlabel("X [m]")
        ax.set_ylabel("Y [m]")

    def plot_total_kinetic_energy(self, ax: Axes, obj_masses):

        v2 = np.power(np.vstack(self._v_abs_history),2)

        ek = obj_masses*v2/2

        total = np.sum(ek, axis=1)

        ax.plot(self._time_labels_ms, total)
        ax.set_title("Total Kinetic energy")
        ax.set_xlabel("Time [ms]")
        ax.set_ylabel("Energy [J]")

    def plot_total_potential_energy(self, ax: Axes,obj_masses, potential_energy_calc: Callable):

        total_potential_energy_of_objects = potential_energy_calc(self._x_history, self._y_history, obj_masses)
        total = np.sum(total_potential_energy_of_objects, axis=1)
        ax.plot(self._time_labels_ms, total)
        ax.set_title("Total potential energy of system")
        ax.set_xlabel("Time [ms]")
        ax.set_ylabel("Energy [J]")


    def plot_total_energy_of_system(self, ax: Axes, obj_masses,potential_energy_calc: Callable, zoom_y:bool=False):

        total_potential_energy_of_objects = potential_energy_calc(self._x_history, self._y_history, obj_masses)
        total_pot = np.sum(total_potential_energy_of_objects, axis=1)
        v2 = np.power(np.vstack(self._v_abs_history),2)
        ek = obj_masses*v2/2
        total_kin = np.sum(ek, axis=1)

        total = total_pot+total_kin
        ax.plot(self._time_labels_ms, total)
        ax.set_title("Total energy of system")
        ax.set_xlabel("Time [ms]")
        ax.set_ylabel("Energy [J]")
        if not zoom_y:
            ax.set_ylim(bottom=0,top=max(total)*1.2)
        
