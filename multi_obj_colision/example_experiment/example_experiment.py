from multi_obj_colision.example_experiment.example_experiment_spec import ExampleExperimentSpec
from multi_obj_colision.graphical.pygame_hnadler import PygameHandler
from multi_obj_colision.physical.data_collector import DataCollector
import matplotlib.pyplot as plt

def run():
    exp_spec = ExampleExperimentSpec()
    dc = DataCollector()
    exp_spec.set_data_collector(dc)
    ph = PygameHandler(experiment=exp_spec,time_speed_factor=0.1,zoom_factor=25,limit_fps=True)
    ph.graphics_loop()
    fig, ax = plt.subplots(2,3)
    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1, wspace=0.4, hspace=0.4)
    dc.plot_x_velocity(ax[0][0])
    dc.plot_y_velocity(ax[1][0])
    dc.plot_x(ax[0][1])
    dc.plot_y(ax[1][1])

    dc.plot_trajectory(ax[0][2])
    dc.plot_total_kinetic_energy(ax[1][2], exp_spec.get_obj_masses())

    plt.show(block=True)

    