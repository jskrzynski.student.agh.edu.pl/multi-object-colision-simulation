import pygame

from multi_obj_colision.physical.experiment_spec import ExperimentSpec



class PygameHandler():
    def __init__(self,experiment:ExperimentSpec, zoom_factor=20, time_speed_factor=0.5, limit_fps=True) -> None:
        #screen_width=1280,screen_height=720
        borders = experiment.get_current_borders()
        system_width = borders.right-borders.left
        system_height = borders.top-borders.bottom
        self.screen_width = system_width * zoom_factor
        self.screen_height = system_height * zoom_factor
        self.experiment = experiment
        self.time_speed_factor = time_speed_factor
        self.zoom_factor = zoom_factor
        self.limit_fps = limit_fps
        self._init_pygame_settings()
        

    def _init_pygame_settings(self):
        pygame.init()
        self.screen = pygame.display.set_mode((self.screen_width, self.screen_height))
        self.clock = pygame.time.Clock()
        self.running = True

    def graphics_loop(self):
        current_time = 0
        compensate_time = 0
        dt_cumulative = 0
        while self.running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
            self._clear_pygame_screen()
            self.experiment.next_stage()
            x,y = self.experiment.get_object_coords()
            # Radius is called in each step to handle also situation when object would grow
            r = self.experiment.get_object_radius()
            self._draw_objects(x,y,r)
            m = self.experiment.get_obj_masses()
            self._print_mass_radius_labels(x,y,m,r)
            dt_cumulative+=self.experiment.get_current_dt()
            self._print_actual_time(round(dt_cumulative*1000))

            time_to_wait = self._waiting_time()
            pygame.time.wait(time_to_wait+compensate_time)
            pygame.display.flip()

            time_passed = 0
            if self.limit_fps:
                time_passed = self.clock.tick(60) 
            else:
                time_passed = self.clock.tick()
            
            current_time += time_passed
            compensate_time = time_to_wait-time_passed
        pygame.quit()
    
    def _clear_pygame_screen(self):
        self.screen.fill("white")
    
    def _draw_objects(self, circle_center_x, circle_center_y, radius):
        for i, spec in enumerate(zip(circle_center_x, circle_center_y, radius)):
            x,y,r = spec
            pyg_x, pyg_y = self._translate_cartesian_cords_to_pygame(x,y)
            pygame.draw.circle(self.screen, self._get_color(i), (pyg_x,pyg_y) , self._scale_radius(r))

    
    def _translate_cartesian_cords_to_pygame(self,cartesian_x, cartesian_y):
        pyg_x = self._translate_cartesian_x_to_pygame(cartesian_x)
        pyg_y = self._translate_cartesian_y_to_pygame(cartesian_y)
        return pyg_x,pyg_y

    def _translate_cartesian_x_to_pygame(self,cartesian_x):
        borders = self.experiment.get_current_borders()
        system_width = borders.right-borders.left
        x_scaling_factor = self.screen_width / system_width
        return cartesian_x * x_scaling_factor

    def _translate_cartesian_y_to_pygame(self,cartesian_y):
        borders = self.experiment.get_current_borders()
        system_height = borders.top-borders.bottom
        y_scaling_factor = self.screen_height / system_height
        return self.screen_height - (cartesian_y * y_scaling_factor)

    def _scale_radius(self, r):
        return r*self.zoom_factor

    def _waiting_time(self)->int:
        dt = self.experiment.get_current_dt()
        milisecods = dt * 1000
        scaled_ms = milisecods * self.time_speed_factor
        return round(scaled_ms)

    def _get_color(self,id):
        # TODO Create bijection id<->color or at least function that for small subset
        # of ids like 30 behaves like bijection
        return "red"

    def _print_actual_time(self, time:int):
        green = (0, 255, 0)
        blue = (0, 0, 128)
        text_str = f"Current simulation time: {time} ms"
        font = pygame.font.Font('freesansbold.ttf', 32)
        text = font.render(text_str, True, green, blue)
        textRect = text.get_rect()
        textRect.center = (self.screen_width // 2,  30)
        self.screen.blit(text, textRect)

    def _print_mass_radius_labels(self, x,y,m,r):
        green = (0, 255, 0)
        blue = (0, 0, 128)
        font = pygame.font.Font('freesansbold.ttf', 12)
        for x_, y_, m_, r_ in zip(x,y,m,r):
            text_str = f"(m: {m_} kg, r: {r_} m)"
            text = font.render(text_str, True, green, blue)
            textRect = text.get_rect()
            textRect.center = self._translate_cartesian_cords_to_pygame(x_,y_)
            self.screen.blit(text, textRect)
