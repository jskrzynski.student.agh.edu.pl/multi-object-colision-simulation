# Multi object colision simulation

## Project descripion

Simulation of multiple coliding objects within a box-like borders of system

## First version

The jupyter notebook contains the first version of simulation that consists works as a library. There is no GUI, but all features are implemented and ready to be used. 

The only dependency of the simulation is numpy

First version is intended to be the foundation for the final version of symulation with GUI for end user. First version provides the most meaningful components with respect to main goal of project.

It contains all the necesairy functions that are going to be used and wraps them with a single class that user may interact with

There are still some question marks about final grooming of code. Some accesibility modifiers may change and still the arguments validation is going to be included

## Second version

This is a ready to use python library. User defines own experiments and the way they are handled. Directory `example_experiment` contains an example how to  use it.

First of all the specification of the system have to be defined as the class derived from `ExperimentSpec`

```python
from typing import Iterable, Tuple, Union
from multi_obj_colision.physical.experiment_spec import ExperimentSpec, SimulationStepType
from multi_obj_colision.physical.multi_object_colision import ObjectsSet, SystemBorders


class ExampleExperimentSpec(ExperimentSpec):

    def __init__(self) -> None:
        obj_set = ObjectsSet()
        radius = [1,2,3]
        mass = [10,10,30]
        x = [2,6,12]
        y = [4,4,4]
        vx = [1,0,0]
        vy = [0,0,0]
        obj_set.add_object(radius,mass,x,y,vx,vy)
        super().__init__(obj_set)

    def _is_next_step_with_acceleration(self, step_id:int)->SimulationStepType:
        """
        Do we accelerate in `step_id` step? (acceeration can be negative)
        And hov the acceleration is computed
        """
        if(step_id < 10):
            return SimulationStepType.ACCELERATION_BY_NUMBER
        else:
            return SimulationStepType.NO_ACCELERATION

    
    def _get_acceleration_value(self, step_id:int)->Tuple[Union[Iterable,float], Union[Iterable,float]]:
        """
        Returns tuple (ax,ay) of iterables containing x and y components of acceleration
        acting in the given timestep

        May not be implemented if system accelerates only by force
        """
        return (0,-10) # gravity

    # def _get_force_value(self, step_id:int)->Tuple[Iterable, Iterable]:
    #     """
    #     Returns tuple (fx,fy) of iterables containing x and y components of force
    #     acting in the given timestep

    #     May not be implemented if system accelerates only by acceleration
    #     """
    #     raise NotImplemented


    def _get_dt_value(self, step_id:int)->float:
        """
        Returns time difference for the given timestep
        """
        return 0.01

    def _get_system_borders(self, step_id:int)->SystemBorders:
        """
        Returns the borders of a system in the given timestep

        When running in graphics mode it would be easier to have it as constant value across timeline
        """
        return SystemBorders(10,25,0,0)
```

It is a wrapper around the physics computations. We have to provide when the acceleration and forces are applied, what are the borders and what is the dt. As these things are functions of step number we may manipulate them as the simulation proceeds.


To have data collected one may use the class `DataCollector`. It should be injected into experiment specification

```python
exp_spec = ExampleExperimentSpec()
dc = DataCollector()
exp_spec.set_data_collector(dc)
```

Then it is also possible to use `PygameHandler` to be able to visualise the experiment. Complete code that runs an experiment with visualisation and ploting some data is presented below.

```python
from multi_obj_colision.example_experiment.example_experiment_spec import ExampleExperimentSpec
from multi_obj_colision.graphical.pygame_hnadler import PygameHandler
from multi_obj_colision.physical.data_collector import DataCollector
import matplotlib.pyplot as plt

exp_spec = ExampleExperimentSpec()
dc = DataCollector()
exp_spec.set_data_collector(dc)
ph = PygameHandler(experiment=exp_spec,time_speed_factor=0.1,zoom_factor=25,limit_fps=True)
ph.graphics_loop()
fig, ax = plt.subplots(2,2)
dc.plot_x_velocity(ax[0][0])
dc.plot_y_velocity(ax[1][0])
dc.plot_x(ax[0][1])
dc.plot_y(ax[1][1])

plt.show(block=True)
```

Or you may just simply run the main file in the repository that uses this exact code wrapped with a nice function.

### Requirements
- pygame
- numpy
- matplotlib