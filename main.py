import multi_obj_colision.example_experiment.example_experiment as exper1
import multi_obj_colision.example_experiment_gravitation.example_experiment as exper2
import multi_obj_colision.example_experiment_newtonian.example_experiment as exper3
import multi_obj_colision.example_experiment_attraction.example_experiment as exper4

exper1.run()
exper2.run()
exper3.run()
exper4.run()